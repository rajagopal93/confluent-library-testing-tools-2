package com.swiggy.kafka.client.Redis;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisClient {

    private static JedisPool jedisPool = null;

    public static JedisPool getInstance(String host, int port) {
        if (jedisPool!=null) {
            return jedisPool;
        }
        return getJedisPool(host,port);
    }

    private static JedisPool getJedisPool(String host, int port) {

        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(2000);
        poolConfig.setMaxIdle(2000);
        poolConfig.setMinIdle(2000);
        poolConfig.setBlockWhenExhausted(true);


        jedisPool = new JedisPool(poolConfig, host, port, 10000);
        return jedisPool;

    }


}
