package com.swiggy.kafka.client.consumer;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.kafka.client.Application;
import com.swiggy.kafka.client.Redis.RedisHelpers;
import com.swiggy.kafka.client.common.Env;
import com.swiggy.kafka.client.pojo.Message;
import com.swiggy.kafka.clients.Record;
import com.swiggy.kafka.clients.consumer.Handler;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MessageHandler implements Handler {
    public static Logger logger = Logger.getLogger(MessageHandler.class.getName());
    public static Gson gson = new Gson();

    public MessageHandler() {
        if (Application.argumentMap.get(Env.LOG_LEVEL).equals("ALL")){
            logger.setLevel(Level.ALL);
        } else {
            logger.setLevel(Level.INFO);
        }
    }

    @Override
    public Status process(Record record) {
        long now = System.currentTimeMillis();
        long expectedProcessingTime = 0;
        if (Integer.parseInt(Application.argumentMap.get(ConsumerEnv.MESSAGE_PROCESSING_TIME))>0) {
            expectedProcessingTime = now + Integer.parseInt(Application.argumentMap.get(ConsumerEnv.MESSAGE_PROCESSING_TIME));
        }

        //Actual message processing
        String message = record.getValue();
        String id = JsonPath.parse(message).read("$['id']").toString();
        logger.fine("Message consumed id: " + id);




        // check for max no of messages to be processed
        int maxNoOfMessagesToBeProcessed = Integer.parseInt(Application.argumentMap.get(Env.MAX_MESSAGES_TO_BE_PROCESSED));
        long currentProcessedCount = SampleConsumer.processedCount.addAndGet(1);

        if (maxNoOfMessagesToBeProcessed > 0 && currentProcessedCount >= maxNoOfMessagesToBeProcessed) {
            SampleConsumer.isProcessed = true;
        }

        //update message id in redis and update the duplicate count
        RedisHelpers.updateConsumerResultsInRedis(id);


        // checking for actual processing time and waiting for the remaining if delay is provided
        long end = System.currentTimeMillis();
        if (expectedProcessingTime>0 && (end-now)<expectedProcessingTime) {
            try {
                Thread.sleep(end-now);
            } catch (Exception e) {

            }
        }

        // update last processed message details in common object to print it at last
        SampleConsumer.lastProcessedMessage = gson.fromJson(record.getValue(),Message.class);
        return Status.SUCCESS;
    }
}
