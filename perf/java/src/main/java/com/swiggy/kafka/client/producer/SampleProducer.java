package com.swiggy.kafka.client.producer;

import com.swiggy.kafka.client.Application;
import com.swiggy.kafka.client.common.Env;
import com.swiggy.kafka.client.common.Helpers;
import com.swiggy.kafka.clients.configs.AuthMechanism;
import com.swiggy.kafka.clients.configs.CommonConfig;
import com.swiggy.kafka.clients.configs.ProducerConfig;
import com.swiggy.kafka.clients.configs.Topic;
import com.swiggy.kafka.clients.configs.enums.ProducerAcks;
import com.swiggy.kafka.clients.producer.Producer;
import scala.App;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SampleProducer {

    public static AtomicLong totalCount = new AtomicLong(0);
    public static AtomicLong successCount = new AtomicLong(0);
    public static AtomicLong errorCount = new AtomicLong(0);
    public static AtomicLong completedCount = new AtomicLong(0);

    public static Logger logger = Logger.getLogger(SampleProducer.class.getName());

    public static void loadProducerDefaults() {
        Application.argumentMap.put(ProducerEnv.NO_OF_THREADS,"1");
        Application.argumentMap.put(ProducerEnv.ACK,"ALL");
        Application.argumentMap.put(ProducerEnv.IS_COMPRESSED,"true");
        Application.argumentMap.put(ProducerEnv.IS_SYNC,"true");
        Application.argumentMap.put(ProducerEnv.MESSAGE_FILE_PATH,"/tmp/11KB.json");
        Application.argumentMap.put(ProducerEnv.IS_CALLBACKS,"false");
        Application.argumentMap.put(ProducerEnv.PRODUCER_CLIENT_ID,"algates_java_perf_producer");
        Application.argumentMap.put(ProducerEnv.NO_OF_RETRIES,"1");
        Application.argumentMap.put(ProducerEnv.MESSAGES_PER_SECOND,"0");
        Application.argumentMap.put(ProducerEnv.EXPECTED_PRODUCED_MESSAGECOUNT,"1");
        Application.argumentMap.put(ProducerEnv.TOLERANCE_PERCENTAGE,"10");
        if (Application.argumentMap.get(Env.LOG_LEVEL).equals("ALL")){
            logger.setLevel(Level.ALL);
        } else {
            logger.setLevel(Level.INFO);
        }

    }

    public static void produce() throws InterruptedException {

        if (Application.argumentMap.get(Env.LOG_LEVEL).equals("ALL")){
            logger.setLevel(Level.ALL);
        } else {
            logger.setLevel(Level.INFO);
        }


        // building a topic
        Topic topic = Helpers.buildTopic();

        // Building cluster configs
        CommonConfig.Cluster primaryCluster = Helpers.buildPrimaryCluster();
        CommonConfig.Cluster secondaryCluster = Helpers.buildSecondaryCluster();

        ProducerConfig producerConfig = Helpers.buildProducerConfig(topic,primaryCluster,secondaryCluster);
        Producer producer = new Producer(producerConfig);
        producer.start();
        Thread.sleep(1000 * 5 * 1);

        //creating thread pool with the uer input
        int noOfThreads = 1;
        try {
           noOfThreads = Integer.parseInt(Application.argumentMap.get(ProducerEnv.NO_OF_THREADS));
        } catch (NumberFormatException numberFormatException) {
            System.exit(0);
        }

        ExecutorService executor = Executors.newFixedThreadPool(noOfThreads);

        //spawn all executors
        for (int i=0; i < noOfThreads; i++) {
            executor.execute(new ProducerExecutor(producer, Application.argumentMap.get(Env.TOPIC_NAME),Boolean.parseBoolean(Application.argumentMap.get(ProducerEnv.IS_SYNC)),
                    Boolean.parseBoolean(Application.argumentMap.get(ProducerEnv.IS_CALLBACKS)),
                            Integer.parseInt(Application.argumentMap.get(Env.RUN_DURATION)),
                            0,Application.argumentMap.get(ProducerEnv.MESSAGE_FILE_PATH),Integer.parseInt(Application.argumentMap.get(ProducerEnv.MESSAGES_PER_SECOND))));
        }

        logger.info("Execution Triggered!!!!!!!");
        Thread.sleep(Integer.parseInt(Application.argumentMap.get(Env.RUN_DURATION)) * 1000);

        while (completedCount.get() < noOfThreads) {

        }

        float tolerancevalue = Integer.parseInt(Application.argumentMap.get(ProducerEnv.TOLERANCE_PERCENTAGE))/100f;
        double expectedMessageCountToleranceValue = Integer.parseInt(Application.argumentMap.get(ProducerEnv.EXPECTED_PRODUCED_MESSAGECOUNT)) - tolerancevalue*Integer.parseInt(Application.argumentMap.get(ProducerEnv.EXPECTED_PRODUCED_MESSAGECOUNT));

        logger.info("Total messages produced / sec --->" + "" + totalCount.get()/(Integer.parseInt(Application.argumentMap.get(Env.RUN_DURATION))));
        long successCountPerSecond = successCount.get()/(Integer.parseInt(Application.argumentMap.get(Env.RUN_DURATION)));
        logger.info("Total Actual success count / sec--->" + "" + successCountPerSecond);
        logger.info("Expected message count / sec---> "+Integer.parseInt(Application.argumentMap.get(ProducerEnv.EXPECTED_PRODUCED_MESSAGECOUNT)));
        if ((successCountPerSecond >= Integer.parseInt(Application.argumentMap.get(ProducerEnv.EXPECTED_PRODUCED_MESSAGECOUNT))) || (successCountPerSecond >= expectedMessageCountToleranceValue)) {
            logger.info("Testcase: PASSED");
        }else{
            logger.info("Testcase: FAILED");
        }

        logger.info("Total error count / sec --->" + "" + errorCount.get()/(Integer.parseInt(Application.argumentMap.get(Env.RUN_DURATION))));


        logger.info("Total messages produced --->" + "" + totalCount.get());
        logger.info("Total success count --->" + "" + successCount.get());
        logger.info("Total error count  --->" + "" + errorCount.get());

        logger.info("Execution Stopped!!!!!!!");
        System.exit(0);

    }


}
