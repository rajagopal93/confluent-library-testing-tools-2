package com.swiggy.kafka.client.common;

public class Env {

    public static final String APPLICATION_TYPE = "--appType";
    public static final String LOG_LEVEL = "--logLevel";

    public static final String PRIMARY_BOOTSTRAPSERVER = "--primaryServer";
    public static final String PRIMARY_AUTH = "--isPrimaryAuthEnabled";
    public static final String PRIMARY_USERNAME = "--primaryUsername";
    public static final String PRIMARY_PASSWORD = "--primaryPassword";

    public static final String SECONDARY_BOOTSTRAPSERVER = "--secondaryServer";
    public static final String SECONDARY_AUTH = "--isSecondaryAuthEnabled";
    public static final String SECONDARY_USERNAME = "--secondaryUsername";
    public static final String SECONDARY_PASSWORD = "--secondaryPassword";

    public static final String TOPIC_NAME = "--topic";
    public static final String FAULT_STRATEGY = "--isFaultToleranceEnabled";
    public static final String RUN_DURATION = "--duration";

    public static final String MAX_MESSAGES_TO_BE_PROCESSED = "--maxMessagesToBeProcessed";

    public static final String REDIS_HOST = "--redisHost";
    public static final String REDIS_PORT = "--redisPort";
    public static final String IS_REDIS_ENABLED = "--isRedisEnabled";


}
